#!/bin/sh
#
# Run this command at top level to bring in all updates to all sub-repositories
#

# default remote
REMOTE=origin

git pull --recurse-submodules ${REMOTE} &&
git submodule foreach -q --recursive 'git checkout $(git config -f "$toplevel/.gitmodules" submodule.$name.branch || echo master);'"git pull ${REMOTE}"

